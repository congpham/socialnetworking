# Project description

This is a simple Survey web application.

## Project architecture

The structure has 3 layers.

It consists of:

* `VueJS` - JS frameworks handles the UI
* `Spring` - backend server handle business flows
* `MongoDB` - No relational database for simple structure

The application deployment using `Docker Compose` with 3 `Containers` for 3 layers.

## How to build run application using `Docker` file

* `Java`, `Maven` and `nodejs` should be installed.

If you're using `Maven`, from the root folder:

1. In `backend` directory, run `#mvn spring-boot:run` to build `jar` file and start backend server.
2. In `frontend` directory, run `#npm install && npm run dev`

## How to run project independently

1. Install Docker `https://www.docker.com/get-started`

2. Open terminal, run `#docker-compose up --build`

3. Profit!


## How to work with the application (For developer)

### Commit and Pull Request

- Create your own branch when working with the project
- *MUST NOT* work directly in `develop` or `master` branch
- When finish, commit the changed source code and create a pull request (PR) to `develop` branch
- Commits and PRs should specify the purpose and has the tag:

`[Frontend]` if relates to UI layer

`[Backend]` if relates to backend layer

`[System]` if relates to Docker or others

### Structure:

VueJS:

Java:

* `src`: core backend code

- `controller` contains restful APIs
- `domain` contains the main models
- `service`  which connects with the databse and doing CRUD
- `repository` provide CRUD machanism (not sure whether it supports MongoDB or not)


## Preferences

sample: https://dev-pages.info/how-to-run-spring-boot-and-mongodb-in-docker-container/

beginner guide: https://medium.com/@deepakshakya/beginners-guide-to-use-docker-build-run-push-and-pull-4a132c094d75

http://blog.thoward37.me/articles/where-are-docker-images-stored/

JPARepository: https://spring.io/blog/2011/02/10/getting-started-with-spring-data-jpa/
