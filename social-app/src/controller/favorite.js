import {List} from "immutable";
import matchesProperty from "lodash/matchesProperty";
import Store from "./store";


function Favorite() {
  this.id = '';
  this.userId = '';
  this.tweetId = '';
  this.date = '';
}

//setters
/**
 * @param {Favorite} item
 * @param {List} list
 */
export function add(list, item) {
  return list.push(item);
}

/**
 * @param {Favorite} item
 * @param {List} list
 */
export function remove(list, item) {
  const index = list.findIndex(matchesProperty("id", item.id));
  return list.delete(index);
}

/**
 *
 * @param {List<Favorite>} list
 * @param {String} userId
 * @returns {List<Favorite>}
 */
export function getByUser(list, userId) {
  return list.filter(i => i.userId === userId);
}

/**
 * @param {List<Favorite>} list
 * @param {String} tweetId
 * @returns {List<Favorite>}
 */
export function getByTweet(list, tweetId) {
  return list.filter(i => i.tweetId === tweetId);
}


export const FavoriteStore = Store({
  key: 'favorites',
  state: List(),
  setters: {add, remove},
  getters: {getByUser, getByTweet}
});
