import {List} from "immutable";
import Store from "./store";


function Follower() {
  this.userId = '';
  this.followerId = '';
  this.date = '';
}

//setters
/**
 * @param {Follower} item
 * @param {List} list
 */
export function add(list, item) {
  return list.push(item);
}

/**
 * @param {Follower} item
 * @param {List<Follower>} list
 */
export function remove(list, item) {
  return list.delete(list.findIndex(i => i.userId === item.userId && i.followerId === item.followerId));
}

/**
 * @param {Follower} item
 * @param {List<Follower>} list
 */
export function findOne(list, item) {
  return list.find(i => i.userId === item.userId && i.followerId === item.followerId);
}

/**
 *
 * @param {List<Follower>} list
 * @param  userId
 * @returns {List<Follower>}
 */
export function getByUser(list, userId) {
  return list.filter(i => i.userId === userId);
}

export function getAllFollowers(list, userId, userStore, tweetStore) {
  return list.filter(i => i.userId === userId)
    .map(user => userStore().findById(user.followerId))
    .map(user => {
      user.tweets = tweetStore().getByUser(user.id).toArray() || [];
      return user;
    });
}

export function getFollowingByUser(list, userId) {
  return list.filter(u => u.followerId === userId);
}

export function getFollowingUsers(list, userId, userStore, tweetStore) {
  return list.filter(u => u.followerId === userId)
    .map(user => userStore().findById(user.userId))
    .filter(user => user && user.id)
    .map(user => {
      user.tweets = tweetStore().getByUser(user.id).toArray() || [];
      return user;
    });
}

export const FollowerStore = Store({
  key: 'followers',
  state: List(),
  setters: {add, remove},
  getters: {getByUser, getFollowingByUser, findOne, getAllFollowers, getFollowingUsers}
});
