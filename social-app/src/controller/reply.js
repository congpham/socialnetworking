import {List} from "immutable";
import Store from "./store";


function Reply() {
  this.userId = '';
  this.tweetId = '';
  this.reply = '';
  this.date = '';
  this.id = '';
}

//setters
/**
 * @param {Reply} item
 * @param {List} list
 */
export function add(list, item) {
  return list.push(item);
}

/**
 * @param {Reply} item
 * @param {List} list
 */
export function remove(list, item) {
  return list.delete(list.findIndex(i => i.tweetId === item.tweetId && i.userId === item.userId));
}

/**
 * @param {List<Reply>} list
 * @returns {List<Reply>}
 */
export function getByUser(list, userId) {
  return list.filter(i => i.userId === userId);
}

/**
 * @param {List<Reply>} list
 * @returns {List<Reply>}
 */
export function getByTweet(list, tweetId) {
  return list.filter(i => i.tweetId === tweetId);
}

export function getByTweetWithUser(list, tweetId, userStore) {
  return list
    .filter(i => i.tweetId === tweetId)
    .map(reply => {
      reply.user = userStore().findById(reply.userId);
      return reply;
    });
}

export const ReplyStore = Store({
  key: 'replies',
  state: List(),
  setters: {add, remove},
  getters: {getByUser, getByTweet, getByTweetWithUser}
});
