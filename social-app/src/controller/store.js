import { List } from "immutable";

function decorateMethods(obj, decorator) {
  let newObject = { ...obj };

  Object.keys(newObject).forEach(function decorateMethod(fnName) {
    if (typeof newObject[fnName] === "function") {
      newObject[fnName] = decorator(newObject[fnName]);
    }
  });
  return newObject;
}

function Store(storeConfig) {
  return function () {
    
    let state = storeConfig.state;
    const key = storeConfig.key;
    if(key){
      const data = localStorage.getItem(key);
      if (data) {
        state = List(JSON.parse(data));
      }
    }

    function setter(fn) {
      return function (...args) {
        state = fn(state, ...args);
        if(key) localStorage.setItem(key, JSON.stringify(Array.from(state)));
        return args.length > 0 ? args[0] : null;
      };
    }

    function getter(fn) {
      return function (...args) {
        return fn(state, ...args);
      };
    }

    return Object.freeze({
      ...decorateMethods(storeConfig.getters, getter),
      ...decorateMethods(storeConfig.setters, setter)
    });
  };
}

export default Store;
