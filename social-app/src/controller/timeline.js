import {TweetStore} from './tweet';
import {UserStore} from './user';
import {FavoriteStore} from './favorite';
import {FollowerStore} from './follower';
import {ReplyStore} from './reply';


class Timeline {
  constructor(options = {}) {
    this.id = '';
    this.content = '';
    this.favorites = [];
    this.replies = [];
    this.user = null;
    Object.assign(this, options)
  }

}

/**
 * get all tweet of userId
 * @param {TweetStore} tweets
 * @param {UserStore} users
 * @param {FavoriteStore} favorites
 * @param {FollowerStore} followers
 * @param {ReplyStore} replies
 * @param {String} userId
 * @returns {List<Tweet>}
 */
const _getTimeline = (tweets, followers, favorites, replies, users, userId) => {
  const userIds = [].concat(followers.getFollowingByUser(userId).map(user => user.userId).toArray());
  return tweets.getByUsers(userIds)
    .sort((i, j) => new Date(j.createdAt) - new Date(i.createdAt))
    .map(t => new Timeline({
      ...t,
      user: users.getOne(t.userId),
      favorites: favorites.getByTweet(t.id).toArray(),
      replies: replies.getByTweet(t.id).toArray()
    })).toArray();
};

const searchHashTags = (tweets, followers, favorites, replies, users, tag) => {
  return tweets.findByTag(tag)
    .sort((i, j) => new Date(j.createdAt) - new Date(i.createdAt))
    .map(t => new Timeline({
      ...t,
      user: users.getOne(t.userId),
      favorites: favorites.getByTweet(t.id).toArray(),
      replies: replies.getByTweet(t.id).toArray()
    })).toArray();
};

/**
 * get all tweet of users those {userId} follow
 * @param {TweetStore} tweets
 * @param {UserStore} users
 * @param {FavoriteStore} favorites
 * @param {FollowerStore} followers
 * @param {ReplyStore} replies
 * @param {String} userId
 * @returns {List<Tweet>}
 */
const _me = (tweets, followers, favorites, replies, users, userId) => {
  return tweets.getByUser(userId)
    .sort((i, j) => j.createdAt - i.createdAt)
    .map(t => new Timeline({
      ...t,
      user: users.getOne(t.userId),
      favorites: favorites.getByTweet(t.id).toArray() || [],
      replies: replies.getByTweet(t.id).toArray() || []
    })).toArray();

};

export const TimelineController = {
  me: (userId) => _me(TweetStore(), FollowerStore(), FavoriteStore(), ReplyStore(), UserStore(), userId),
  timeline: (userId) => _getTimeline(TweetStore(), FollowerStore(), FavoriteStore(), ReplyStore(), UserStore(), userId),
  searchByHashTag: (tag) => searchHashTags(TweetStore(), FollowerStore(), FavoriteStore(), ReplyStore(), UserStore(), tag)
};
