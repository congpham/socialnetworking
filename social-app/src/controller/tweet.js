import {List} from "immutable";
import Store from "./store";


function Tweet() {
  this.id = '';
  this.userId = '';
  this.tweet = '';
  this.createdAt = '';
}

//setters
/**
 * @param {Tweet} item
 * @param {List} list
 */
export function add(list, item) {
  return list.push(item);
}

/**
 * @param {Tweet} item
 * @param {List} list
 */
export function remove(list, item) {
  return list.delete(list.findIndex(i => i.id === item.id));
}

//getters
/**
 *
 * @param {List<Tweet>} list
 * @param {String} userId
 * @returns {List<Tweet>}
 */
export function getByUser(list, userId) {
  return list.filter(i => i.userId === userId);
}

/**
 *
 * @param {List<Tweet>} list
 * @param {Array} users
 * @returns {List<Tweet>}
 */
export function getByUsers(list, users) {
  return list.filter(i => users.indexOf(i.userId) >= 0);
}

export const findById = (list, id) => list.find(tweet => tweet && tweet.id === id);
export const findByTag = (list, tag) => list.filter(tweet => tweet && tweet.tweet.split(`#${tag}`).length > 1);

export const TweetStore = Store({
  key: 'tweets',
  state: List(),
  setters: {add, remove},
  getters: {getByUser, getByUsers, findById, findByTag}
});
