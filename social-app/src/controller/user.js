import {List} from "immutable";
import partial from "lodash/partial";
import matchesProperty from "lodash/matchesProperty";
import Store from "./store";
import {FollowerStore} from "@/controller/follower";

function User() {
  this.id = '';
  this.name = '';
  this.username = '';
  this.password = '';
}

//setters
/**
 * @param {List} list list of item.
 * @param {User} item
 * @returns {List<User>}
 */
export function add(list, item) {
  return list.push(item);
}

/**
 * @param {List} list list of item.
 * @param {User} item
 * @returns {List<User>}
 */
export function remove(list, item) {
  const index = list.findIndex(matchesProperty("id", item.id));
  return list.delete(index);
}

//getters
/**
 * @param {List} list
 * @returns {User}
 */
export function getOne(list, id) {
  return list.find(i => i.id === id);
}

/**
 * @param {List} list
 * @param {String} username
 * @returns {User}
 */
export function getByUsername(list, username) {
  return list.find(u => u.username === username);
}

/**
 * @param {List} list
 * @param {String} id
 * @returns {List<User>}
 */
export function getWhoToFollow(list, id) {
  return list
    .filter(u => u.id !== id && !FollowerStore().getFollowingByUser(id).map(item => item.userId).contains(u.id))
    .take(5);
}

export function login(list, username, password) {
  return list.find(i => i.username === username && i.password === password)
}

export const findByUsername = (list, username) => list.find(user => user && user.username === username);
export const findById = (list, id) => list.find(user => user && user.id === id);
export const getUsers = (list, users) => list.find(user => user && users.indexOf(user.id));

export function update(list, user) {
  return list.update(user);
}

export const UserStore = Store({
  key: 'users',
  state: List(),
  setters: {add, remove, update},
  getters: {getOne, login, getByUsername, getWhoToFollow, findByUsername, findById, getUsers}
});
