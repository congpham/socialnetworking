import moment from 'moment'
import Vue from 'vue'
import VeeValidate from 'vee-validate'
import App from './App'
import router from './router'

// register vee validate plugin
Vue.use(VeeValidate)

Vue.config.productionTip = false

// Global filter to format dates with momentjs
Vue.filter('timeAgo', date => moment(date).fromNow())
Vue.filter('joined', date => moment(date).format('MMMM YYYY'))
Vue.filter('dob', date => moment(date).format('MMMM Do YYYY'))
// Vue.filter('withHashtag', data => {
//   const matches = data.match(/#(\S*)/g);
//
//   for (const match of matches) {
//     // data = data.split(match).join(Vue.compile(`<router-link :to="/hashtag/${match.replace('#', '')}">${match}</router-link>`));
//     // data = data.split(match).join(Vue.compile(`<a href="/hashtag/${match.replace('#', '')}">${match}</a>`));
//     data = data.replace(match,`<a href="/hashtag/${match.replace('#', '')}">${match}</a>`)
//   }
//   return data;
// });

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {App}
});
