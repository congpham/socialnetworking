import { TimelineController } from "@/controller/timeline.js"
import { UserStore } from "@/controller/user.js"
import { FollowerStore } from "@/controller/follower.js"
import { FavoriteStore } from "@/controller/favorite.js"

export default {
  created () {
    this.$_profileMixin_fetchUser(this.$route.params.username)

    const token = localStorage.getItem('tweetr-token')

    if (token) {
      this.$_profileMixin_fetchAuthenticatedUser(token)
    }
  },
  methods: {
    $_profileMixin_fetchUser (username) {
      if (username) {
        this.user = UserStore().getByUsername(username);
        this.user.tweets = this.tweets;
        this.user.following = FollowerStore().getFollowingByUser(this.user.id).toArray();
        this.user.followers = FollowerStore().getByUser(this.user.id).toArray();
        this.user.favorites = FavoriteStore().getByUser(this.user.id).toArray();
        this.tweets = TimelineController.timeline(this.user.id);
      } else {
        const token = localStorage.getItem('tweetr-token')

        this.user = UserStore().getOne(token);
      }
    },
    $_profileMixin_fetchAuthenticatedUser (token) {
      this.authUser = UserStore().getOne(token);
    }
  }
}
