
import { List } from 'immutable';
import { add, remove, getByUser, getByTweet } from "@/controller/favorite.js";

describe('favorite.js', () => {
  let list;
  beforeEach(function() {
    // runs before each test in this block
    list = List([{
      id: 'f1',
      userId: "abc@gmail.com",
      tweetId: 't1',
      date: Date.now()
    }])
  });
  describe('add()',()=>{
    it('should not change the input list', () => {
      add(list,{});
      expect(list.count()).to.equal(1);})
    it('should return correct list', () => {
      const list2 = add(list,{
        id: 'f1',
        userId: "abc@gmail.com",
        tweetId: 't1',
        date: Date.now()
      });
      expect(list2.get(0).id).to.equal('f1');})
  });
  describe('remove()',()=>{
    it('should not change the input list', () => {
      add(list,{id:'f1'});
      expect(list.count()).to.equal(1);})
    it('should return correct list', () => {
      const list2 = remove(list,{id: 'f1',});
      expect(list2.count()).to.equal(0);})
  });
  describe('getByUser()',()=>{
    it('should return empty list when username is not present', () => {
      const fv = getByUser(list, 'username');
      expect(fv) == null;})
    it('should return correct list', () => {
      const fv = getByUser(list, 'abc@gmail.com');
      expect(fv.id) === 'f1';})
  });
  describe('getByTweet()',()=>{
    it('should return empty list when no followers', () => {
      const list2 = getByTweet(list, 't2');
      expect(list2.count()).to.equal(0);})
    it('should return correct list', () => {
      const list2 = getByTweet(list, 't1');
      expect(list2.count()).to.equal(1);})
  });
})
