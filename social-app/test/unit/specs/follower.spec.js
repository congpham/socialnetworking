
import { List } from 'immutable';
import { add, remove, findOne, getByUser, getFollowingByUser, getFollowingUsers, getAllFollowers } from "@/controller/follower.js";

describe('follower.js', () => {
  let list;
  beforeEach(function() {
    // runs before each test in this block
    list = List([{
      userId: "abc@gmail.com",
      followerId: 'u1',
      date: Date.now()
    }])
  });
  describe('add()',()=>{
    it('should not change the input list', () => {
      add(list,{});
      expect(list.count()).to.equal(1);})
    it('should return correct list', () => {
      const list2 = add(list,{
        userId: "abc@gmail.com",
        followerId: 'u1',
        date: Date.now()
      });
      expect(list2.get(0).userId).to.equal('abc@gmail.com');})
  });
  describe('remove()',()=>{
    it('should not change the input list', () => {
      add(list,{userId:'abc@gmail.com'});
      expect(list.count()).to.equal(1);})
    it('should return correct list', () => {
      const list2 = remove(list,{userId: 'abc@gmail.com', followerId: "u1"});
      expect(list2.count()).to.equal(0);})
  });
  describe('findOne()',()=>{
    it('should return empty list when username is not present', () => {
      const user = findOne(list, {});
      expect(user) == null;})
    it('should return correct list', () => {
      const fl = findOne(list, {userId: "abc@gmail.com", followerId: "u1"});
      expect(fl.userId) === 'abc@gmail.com';})
  });
  describe('getByUser()',()=>{
    it('should return empty list when username is not present', () => {
      const user = getByUser(list, 'username');
      expect(user) == null;})
    it('should return correct list', () => {
      const user = getByUser(list, 'abc@gmail.com');
      expect(user.followerId) === 'u1';})
  });
  describe('getFollowingByUser()',()=>{
    it('should return empty list when no followers', () => {
      const list2 = getFollowingByUser(list, 'u2');
      expect(list2.count()).to.equal(0);})
    it('should return correct list', () => {
      const list2 = getFollowingByUser(list, 'u1');
      expect(list2.count()).to.equal(1);})
  });
  describe('getFollowingUsers()',()=>{
    it('should return empty list when no followers', () => {
      const list2 = getFollowingUsers(list, 'u2');
      expect(list2.count()).to.equal(0);})
    it('should return correct list', () => {
      const list2 = getFollowingUsers(list, 'u1');
      expect(list2.count()).to.equal(1);})
  });
  describe('getAllFollowers()',()=>{
    it('should return empty list when no followers', () => {
      const list2 = getAllFollowers(list, 'u2');
      expect(list2.count()).to.equal(0);})
    it('should return correct list', () => {
      const list2 = getAllFollowers(list, 'u1');
      expect(list2.count()).to.equal(1);})
  });
})
