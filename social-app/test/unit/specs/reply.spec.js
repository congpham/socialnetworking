
import { List } from 'immutable';
import { add, remove, getByTweet, getByUser, getByTweetWithUser } from "@/controller/reply.js";

describe('tweet.js', () => {
  let list, item = {
    userId: 'userid',
    tweetId: 'tweetid',
    reply: "This is a reply for tweet1"
  };
  beforeEach(function () {
    // runs before each test in this block
    list = List([item])
  });
  describe('add()', () => {
    it('should not change the input list', () => {
      add(list, {});
      expect(list.count()).to.equal(1);
    })
    it('should return correct list', () => {
      const list2 = add(list, {
        userId: 'userid1',
      });
      expect(list2.get(1).userId).to.equal('userid1');
    })
  });
  describe('remove()', () => {
    it('should not change the input list', () => {
      add(list, item);
      expect(list.count()).to.equal(1);
    })
    it('should return correct list', () => {
      const list2 = remove(list, item);
      expect(list2.count()).to.equal(0);
    })
  });
  describe('getByUser()', () => {
    it('should return empty list when useId is not present', () => {
      const list2 = getByUser(list, 'userid____');
      expect(list2.count()).to.equal(0);
    })
    it('should return correct list', () => {
      const list2 = getByUser(list, 'userid');
      expect(list2.get(0).userId).to.equal('userid');
    })
  });
  describe('getByTweet()', () => {
    it('should return empty list when tweetId is not present', () => {
      const list2 = getByTweet(list, 'hello');
      expect(list2.count()).to.equal(0);
    })
    it('should return correct list', () => {
      const list2 = getByTweet(list, 'tweetid');
      expect(list2.get(0).tweetId).to.equal('tweetid');
    })
  });
  describe('getByTweetWithUser()', () => {
    it('should return empty list when useId is not present', () => {
      const list2 = getByTweetWithUser(list, 'userid____');
      expect(list2.count()).to.equal(0);
    })
    it('should return correct list', () => {
      const list2 = getByTweetWithUser(list, 'userid');
      expect(list2.get(0).userId).to.equal('userid');
    })
  });
})
