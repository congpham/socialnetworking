
import { List } from 'immutable';
import { TimelineController } from "@/controller/timeline.js";
import { UserStore } from "@/controller/user.js";
import { Tweet, TweetStore } from "@/controller/tweet.js";
import { ReplyStore } from "@/controller/reply.js";
import { FollowerStore } from "@/controller/follower";

describe('timeline.js', () => {
  let sandbox;
  //sinon
  beforeEach(function () {
    sandbox = sinon.sandbox.create();
    var store = {};
    sandbox.stub(window.localStorage, 'getItem').callsFake(function (key) {
      return store[key];
    });
    sandbox.stub(window.localStorage, 'setItem').callsFake(function (key, value) {
      return store[key] = value + '';
    });
    TweetStore().add({ userId: 'userid', tweet: '', createdAt: 1 });
    TweetStore().add({ userId: 'userid', tweet: '', createdAt: 2 });
  });
  afterEach(function () {
    sandbox.restore();
  });
  describe('timeline()', () => {
    it('should return empty list when useId is not present', () => {
      const list2 = TimelineController.timeline(['wrong-userid']);
      expect(list2.length).to.equal(0);
    })
    // it('should return correct list', () => {
    //   const list2 = TimelineController.timeline(['userid']);
    //   expect(list2[0].userId).to.equal('userid');
    // })
  });
  describe('me()', () => {
    beforeEach(function () {
      TweetStore().add({ userId: 'followerId', tweet: '', createdAt: 1 });
      TweetStore().add({ userId: 'followerId', tweet: '', createdAt: 2 });
      FollowerStore().add({ userId: 'userid', followerId: 'followerId' });

    });
    it('should return empty list when useId is not present', () => {
      const list2 = TimelineController.me(['wrong-userid']);
      expect(list2.length).to.equal(0);
    })
    // it('should return correct list', () => {
    //   const list2 = TimelineController.me(['userid']);
    //   expect(list2.length).to.equal(2);
    //   expect(list2[0].userId).to.equal('followerId');
    //   expect(list2[0].createdAt).to.equal(2);
    // })
  });
})
