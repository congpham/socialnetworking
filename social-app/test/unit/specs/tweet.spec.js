
import { List } from 'immutable';
import { add, remove, getByUser, getByUsers, findById, findByTag } from "@/controller/tweet.js";

describe('tweet.js', () => {
  let list;
  beforeEach(function() {
    // runs before each test in this block
    list = List([{
      id: '11',
      tweet: "Tweet",
      userId: 'userid',
      createdAt: Date.now()
    }])
  });
  describe('add()',()=>{
    it('should not change the input list', () => {
      add(list,{});
      expect(list.count()).to.equal(1);
    })
    it('should return correct list', () => {
      const list2 = add(list,{
        id: '1',
        tweet: "This is a tweet of user1",
        userId: 'userid',
        createdAt: Date.now()
      });
      expect(list2.get(1).id).to.equal('1');
    })
  });
  describe('remove()',()=>{
    it('should not change the input list', () => {
      add(list,{id:'11'});
      expect(list.count()).to.equal(1);
    })
    it('should return correct list', () => {
      const list2 = remove(list,{id: '11',});
      expect(list2.count()).to.equal(0);
    })
  });
  describe('getByUser()',()=>{
    it('should return empty list when useId is not present', () => {
      const list2 = getByUser(list, 'userid____');
      expect(list2.count()).to.equal(0);
    })
    it('should return correct list', () => {
      const list2 = getByUser(list, 'userid');
      expect(list2.get(0).userId).to.equal('userid');
    })
  });
  describe('findById()',()=>{
    it('should return empty list when useId is not present', () => {
      const list2 = findById(list, 'tw1');
      expect(list2.count()).to.equal(0);
    })
    it('should return correct list', () => {
      const list2 = findById(list, '11');
      expect(list2.count()).to.equal(1);
    })
  });
  describe('findByTag()',()=>{
    it('should return empty list when useId is not present', () => {
      const list2 = findByTag(list, 'userid____');
      expect(list2.count()).to.equal(0);
    })
    it('should return correct list', () => {
      const list2 = findByTag(list, 'userid');
      expect(list2.get(0).userId).to.equal('userid');
    })
  });
  describe('getByUsers()',()=>{
    it('should return empty list when useId is not present', () => {
      const list2 = getByUsers(list, ['userid____']);
      expect(list2.count()).to.equal(0);
    })
    it('should return correct list', () => {
      const list2 = getByUsers(list, ['userid']);
      expect(list2.get(0).userId).to.equal('userid');
    })
  });
})
