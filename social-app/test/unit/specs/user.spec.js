
import { List } from 'immutable';
import { add, remove, getOne, update, login, getByUsername, getWhoToFollow, findByUsername, findById, getUsers } from "@/controller/user.js";

describe('user.js', () => {
  let list;
  beforeEach(function() {
    // runs before each test in this block
    list = List([{
      id: 'u1',
      username: "abc@gmail.com",
      password: 'Admin@123',
      name: 'Cong Pham'
    }])
  });
  describe('add()',()=>{
    it('should not change the input list', () => {
      add(list,{});
      expect(list.count()).to.equal(1);})
    it('should return correct list', () => {
      const list2 = add(list,{
        id: 'u1',
        username: "abc@gmail.com",
        password: 'Admin@123',
        name: 'Cong Pham'
      });
      expect(list2.get(0).id).to.equal('u1');})
  });
  describe('remove()',()=>{
    it('should not change the input list', () => {
      add(list,{id:'u1'});
      expect(list.count()).to.equal(1);})
    it('should return correct list', () => {
      const list2 = remove(list,{id: 'u1',});
      expect(list2.count()).to.equal(0);})
  });
  describe('getOne()',()=>{
    it('should return empty list when username is not present', () => {
      const user = getOne(list, 'userid');
      expect(user) == null;})
    it('should return correct list', () => {
      const user = getOne(list, 'u1');
      expect(user.username) === 'abc@gmail.com';})
  });
  describe('findById()',()=>{
    it('should return empty list when username is not present', () => {
      const user = findById(list, 'userid');
      expect(user) == null;})
    it('should return correct list', () => {
      const user = findById(list, 'u1');
      expect(user.username) === 'abc@gmail.com';})
  });
  describe('getByUsername()',()=>{
    it('should return empty list when username is not present', () => {
      const user = getByUsername(list, 'username');
      expect(user) == null;})
    it('should return correct list', () => {
      const user = getByUsername(list, 'abc@gmail.com');
      expect(user.id) === 'u1';})
  });
  describe('findByUsername()',()=>{
    it('should return empty list when username is not present', () => {
      const user = findByUsername(list, 'username');
      expect(user) == null;})
    it('should return correct list', () => {
      const user = findByUsername(list, 'abc@gmail.com');
      expect(user.id) === 'u1';})
  });
  describe('getUsers()',()=>{
    it('should return empty list when username is not present', () => {
      const list2 = getUsers(list, []);
      expect(list2.count()).to.equal(0);})
    it('should return correct list', () => {
      const list2 = getUsers(list, [{id: 'u1'}]);
      expect(list2.count()).to.equal(1);})
  });
  describe('getWhoToFollow()',()=>{
    it('should return empty list when no followers', () => {
      const list2 = getWhoToFollow(list, 'u1');
      expect(list2.count()).to.equal(0);})
    it('should return correct list', () => {
      const list2 = getWhoToFollow(list, 'u2');
      expect(list2.count()).to.equal(1);})
  });
  describe('update()',()=>{
    it('should return empty list when username is not present', () => {
      const user = update(list, {});
      expect(user) == null;})
    it('should return correct list', () => {
      const user = update(list, {
        id: 'u1',
        username: "abcd@gmail.com",
        password: 'Admin@123',
        name: 'Cong Pham'
      });
      expect(user.username) === 'abcd@gmail.com';})
  });
  describe('login()',()=>{
    it('should return empty list when username is not present', () => {
      const user = login(list, 'userid', 'password');
      expect(user) == null;})
    it('should return correct list', () => {
      const user = login(list, 'abc@gmail.com', 'Admin@123');
      expect(user.username) === 'abc@gmail.com';})
  });
})
